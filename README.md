# Chef role cookbook bootstrap pipeline

## Description

Jenkins pipeline creates a Bitbucket repository , bootstraps Chef role cookbook , pushes changes back to Bitbucket repository and notifies email recipients.

Further details can be found http://web.global.nibr.novartis.net/apps/confluence/pages/resumedraft.action?draftId=111918054&draftShareId=78e50aeb-799b-4f6d-8862-80ba81a3dd88 



