/*

Description: 
-------------

Jenkins declarative pipeline creates a Chef role cookbook repository Bitbucket repository, generates files and commits back all changes.

Parameters:
-------------

Pipeline expect as input three arguments:

ROLE_NAME 

- is a name of Chef role cookbook 
- parameter expects a name starting with role_ fails otherwise
- role cookbook uses this variable as a name for the role cookbook
- must start with role_<APPLICATION NAME>
- is validated for role_ pattern

ROLE_DESCRIPTION

- parameter should be APPLICATION NAME
- is limited to 5 characters

ROLE_MAINTAINER_EMAIL 

- parameter expects an email 
- does a is simple pattern match validation
- fails if the default email is used

Post action
-------------

The pipeline calls as a last stage step another Jenkins job

*/




/*/////////////////////////////////////////////////////////////////
  Variables section
*//////////////////////////////////////////////////////////////////

// constants 
ROLE_DEPENDENCY = 'nx_core'
ROLE_VERSION    = '0.1.0'

// Build info
BUILD_WORKING_DIR = "build_" + BUILD_NUMBER

// Global proxy settings for shell commands
HTTPS_PROXY='https_proxy=http://nibr-proxy.global.nibr.novartis.net:2011'


/*
  Bitbucket global settings
*/

// path in BB
BITBUCKET_SLUG = 'nibr_linux_servers'
// username in BB 
BITBUCKET_USERNAME = 'nibr_linux_servers'
// name of default branch
BITBUCKET_BRANCH_NAME = 'master'

// automation account
BITBUCKET_CRED_ID = 'BITBUCKET_AUTOMATION_ID'

//
BITBUCKET_GROUP_ACCESS = 'rcp-linux-admins'


/*
   Global mail settings
*/

// fallback email recipient in case of failed build
INFO_EMAIL = 'marek.ciganek@novartis.com'

/*
  Notifications
*/
NOTIFY_EMAIL = true



/*
   Debugging 
*/

DEBUG = 0

/*
  Pipeline project
*/
BUILD_PIPELINE_JOB_NAME = 'unified-chef/public/cookbook-watch-for-updates'


/*/////////////////////////////////////////////////////////////////
  Functions section  
*//////////////////////////////////////////////////////////////////

def validate_parameter_input(name){
    def jenkins_parameter = name
    return ( ! jenkins_parameter.startsWith("role_") ) ? false : true
}

def validate_parameter_email(email){
    def user_email = email
    if ( ! user_email  =~ /[_A-Za-z0-9-]+(.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(.[A-Za-z0-9]+)*(.[A-Za-z]{2,})/  || user_email =~ /test@example.com/ ){
        currentBuild.result   = 'FAILED'
        return false
    } else{ 
        return true
    }        
}
// simple check if role description is short enought 
def validate_parameter_descripton(description){
    def role_description = description
    // limit string length to 5 characters
    return ( role_description.length() <= 5 ) ? true : false
}

// email notification in a following format
/*
Build details:

JOB NAME: rcp-nxcore/ciganma1-chef-boostrap-role-cookbook-pipeline
BUILD ID: 476
NODE NAME: master
URL: http://nrchbs-sld4019.nibr.novartis.net:8080/job/rcp-nxcore/job/ciganma1-chef-boostrap-role-cookbook-pipeline/476/
*/

def send_email_notification(rcp,stat){
    def recipient       = rcp
    def build_status    = stat
    def mail_body       = """
Build details:\n\n\n
JOB NAME: ${env.JOB_NAME}
BUILD ID: ${env.BUILD_ID}
NODE NAME: ${env.NODE_NAME}
URL: ${env.BUILD_URL}
RUN DISPLAY: ${env.RUN_DISPLAY_URL}
"""


    if ( build_status == 'Successful') { mail_body = "${mail_body}GIT REPO: http://bitbucket.org/${BITBUCKET_SLUG}/${ROLE_NAME}.git" }

    if ( recipient.size() == 0 ) { return } 

    mail(to: recipient,
        subject: "${env.JOB_NAME} BUILD: ${env.BUILD_ID} ${build_status}",
        body: mail_body
        )
}

/*/////////////////////////////////////////////////////////////////
  Pipeline section
*//////////////////////////////////////////////////////////////////


pipeline {
    agent any
    parameters {
            string(
                name: 'ROLE_NAME',
                defaultValue:"role_<APPLICATION>",
                description: "Name of a new role in a format role_<APPLICATION> . Mandatory parameter.")
            string(
                name: 'ROLE_DESCRIPTION',
                defaultValue:"<APPLICATION>",
                description: "Description of RCP Application stack. \n\nExample:\ne.g. LPD")
            string(
                name: 'ROLE_MAINTAINER_EMAIL',
                defaultValue:"test@example.com",
                description: "Maintainer email. Don't use default value build will fail.")    
    }
    
    // Individual pipeline stages
    stages {

        /*
           Stage validates input parametes
        */
        stage('Validation'){
            steps {
                script {

                        if ( validate_parameter_input(params.ROLE_NAME) == false ){
                            error("Error! ${ROLE_NAME} doesn't follow naming convention. The role name must start with role_")
                            currentBuild.result='FAILES'
                        }

                        if ( validate_parameter_email(params.ROLE_MAINTAINER_EMAIL) == false ){
                            error("Error! ${ROLE_MAINTAINER_EMAIL} is not valid or you provided the default email.")
                            currentBuild.result='FAILED'
                        }
                        if ( validate_parameter_descripton(params.ROLE_DESCRIPTION) == false ){
                            error("Error! ${ROLE_DESCRIPTION} is not valid description for application.")
                            currentBuild.result='FAILED'
                        }
                    }
            }
        }

        /*
           Stage runs BItbucket API  2.0 call to check if REPO_NAME doesn't exists 
        */

        stage('Check Chef role cookbook repository'){
            steps {
                script {
                    //with stored Jenkins credentials we push our changes to Bitbucket
                    withCredentials([usernamePassword(credentialsId: BITBUCKET_CRED_ID , passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                        def output_repository_check = sh(returnStdout: true, script: "${HTTPS_PROXY}  curl  -u ${GIT_USERNAME}:${GIT_PASSWORD} https://api.bitbucket.org/2.0/repositories/" + BITBUCKET_SLUG  + "/$ROLE_NAME | jq \'.error.message\' ")
                    
                        if ( DEBUG ) { print "DEBUG: ${output_repository_check}" }
                            

                        if ( output_repository_check =~ /not found/ ) { 
                            currentBuild.result='SUCCESS'
                            return 
                        } else {
                             error("Error! Repository ${ROLE_NAME} has been found on Bitbucket. Please choose another name." )
                              currentBuild.result='FAILED'
                        }

                    }
                }
            }
        }

        /*
           Stage creates a new Bitbucket repository with a name defined as ROLE_NAME parameter
        */
        
        stage('Create Chef role cookbook repository'){
            when {
                    expression {
                            currentBuild.result == 'SUCCESS' 
                    }
            }
            steps {
                   
                script{
                        
                    withCredentials([usernamePassword(credentialsId: BITBUCKET_CRED_ID , passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                        // Bitbucket API call to create a repository
                        def output_repository_creation = sh(returnStdout: true, script: "${HTTPS_PROXY} curl  -u ${GIT_USERNAME}:${GIT_PASSWORD} https://api.bitbucket.org/2.0/repositories/" + BITBUCKET_SLUG  + "/${ROLE_NAME}   -d '{\"scm\": \"git\",\"is_private\": \"true\",\"fork_policy\":\"no_public_forks\",\"name\":\"'$ROLE_NAME'\"}'")
   
                        if ( DEBUG ) { print "DEBUG: ${output_repository_creation}" }

                        if ( output_repository_creation =~ /You cannot administer other userspersonal accounts/ ){
                            error("Error! Unable to create ${ROLE_NAME} on Bitbucket. Please use another account" )
                            currentBuild.result='FAILED'
                        }else{
                            currentBuild.result='SUCCESS'
                        }

                    }
                }
            }
        }
        
        /*
          Stage creates a master branch in the new repository via Bitbucket API 2.0 call 
          This step is necessary for the next stage if not created the next steps fails 
        */ 

        stage('Create Chef role cookbook create branch access'){
            when {
                    expression {
                            currentBuild.result == 'SUCCESS' 
                    }
            }
            steps {
                script{
                    withCredentials([usernamePassword(credentialsId: BITBUCKET_CRED_ID , passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                        // Bitbucket API call to create branch from newly initiated  repository]
                        def output_repository_branch = sh(returnStdout: true, script: "${HTTPS_PROXY} curl  -u ${GIT_USERNAME}:${GIT_PASSWORD} https://api.bitbucket.org/2.0/repositories/" + BITBUCKET_SLUG  + "/${ROLE_NAME}/src   -F \"branch=${BITBUCKET_BRANCH_NAME}\" " )
  
                        if ( DEBUG ) { print "DEBUG: ${output_repository_branch}" }
                         
                        if ( output_repository_branch  =~ /You cannot administer other userspersonal accounts/ ){
                            error("Error! Unable to create ${ROLE_NAME} on Bitbucket. Please use another account" )
                            currentBuild.result='FAILED'
                        }else{
                            currentBuild.result='SUCCESS'
                        }
                    }
                }
            }
        }

       /*
          Stage grant access via Bitbucket API 1.0 call 
        */ 

        stage('Grant group  access to repository'){
            when {
                    expression {
                            currentBuild.result == 'SUCCESS' 
                    }
            }
            steps {
                script{
                    withCredentials([usernamePassword(credentialsId: BITBUCKET_CRED_ID , passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                        // Bitbucket API call to create branch from new repository
                        def output_api_access  = sh(returnStdout: true, script: "${HTTPS_PROXY} curl  -X PUT -u ${GIT_USERNAME}:${GIT_PASSWORD} https://api.bitbucket.org/1.0/group-privileges/${BITBUCKET_SLUG}/${ROLE_NAME}/${BITBUCKET_SLUG}/${BITBUCKET_GROUP_ACCESS} --data admin"  )
  
                        if ( DEBUG ) { print "DEBUG: ${output_api_access}" }
                         
                        if ( output_api_access  =~ /No Group matches the given query./ ){
                            /*
                                RE-ENABLE and remove currentBuild.result='SUCCESS' parameter setting
                            */
                            //error("Error! Can't find ${BITBUCKET_GROUP_ACCESS} on Bitbucket. Please check if the group is correct." )
                            //currentBuild.result='UNSTABLE'
                            currentBuild.result='SUCCESS'
                        }else{
                            currentBuild.result='SUCCESS'
                        }
                    }
                }
            }
        }


        /*
          Stage pulls newly created repository from the Bitbucket repository 

        */

        stage('Clone newly created Chef role cookbook repository'){
            //steps {
            when {
                    expression {
                            currentBuild.result == 'SUCCESS' 
                    }
            }
            steps { 
                script {
                    deleteDir()
                }
                dir("${BUILD_WORKING_DIR}/${ROLE_NAME}" ){
                        git credentialsId: BITBUCKET_CRED_ID , url: "https://bitbucket.org/" + BITBUCKET_SLUG  + "/" + ROLE_NAME + ".git" ,  changelog: false , poll: false
                }
            }
                  
        }

        /*
           Stage generates Chef role cookbook files based on input parameters
        */
        stage('Generate new Chef role cookbook'){
            when {
                    expression {
                            currentBuild.result == null || currentBuild.result == 'SUCCESS' 
                    }
            }
            steps {
                script {
                  // generate README.md file
                   dir("${BUILD_WORKING_DIR}"){
                       
                    def output_template_creation = sh (returnStdout: true, script: """

                    cat > ${ROLE_NAME}/metadata.rb <<EOF_metadata
name '${ROLE_NAME}' 
maintainer '${ROLE_MAINTAINER_EMAIL}'
maintainer_email '${ROLE_MAINTAINER_EMAIL}'
license 'Proprietary - All Rights Reserved'
source_url 'https://bitbucket.org/novartisnibr/${ROLE_NAME}'
description '${ROLE_DESCRIPTION}'
long_description '${ROLE_DESCRIPTION}'
version '${ROLE_VERSION}'
depends '${ROLE_DEPENDENCY}'
EOF_metadata

                  cat >  ${ROLE_NAME}/README.md <<EOF_readme
Chef role cookbook for ${ROLE_NAME}
EOF_readme
                   
                  [[ ! -d ${ROLE_NAME}/recipies ]] && mkdir -p ${ROLE_NAME}/recipies

                  cat > ${ROLE_NAME}/recipies/default.rb <<EOF_default
# servers linked to the ${ROLE_NAME} are nxcore-managed
include_recipe 'nx_core'
EOF_default

                 cat > ${ROLE_NAME}/recipies/data_site_global.rb <<EOF_global
# the attributes defined here apply to all the machines attached to the ${ROLE_DESCRIPTION}  project, on all sites.
EOF_global

                    """) //end script
                
                    if ( DEBUG ) { print "DEBUG: ${output_template_creation}" }


                   }
                  
                } 

            }
        }

       
        /*
           Stage commits changes to the Bitbucket repository created earlier
        */
        stage('Commit changes to Bitbucket'){
            when {
                    expression {
                            currentBuild.result == null || currentBuild.result == 'SUCCESS' 
                    }
            }
            steps {      
                 script{
                      dir("${BUILD_WORKING_DIR}/${ROLE_NAME}" ){
                        
                        def output_git_push = sh(returnStdout: true,script: """

                                export ${HTTPS_PROXY}
                
                                git config --global user.name ""
                                git config --global user.email ${ROLE_MAINTAINER_EMAIL}
                                git config --global user.name  ${BITBUCKET_USERNAME}
                                git config --global push.default simple              
                        """).trim()
                    
                          if ( DEBUG ) { print "---- DEBUG: ${output_git_push}" }

                        //with stored Jenkins credentials we push our changes to Bitbucket
                        withCredentials([usernamePassword(credentialsId: BITBUCKET_CRED_ID , passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                          sh("${HTTPS_PROXY} git add . ")
                          sh("${HTTPS_PROXY} git commit  -a -m \"Initial commit of ${ROLE_NAME}\"")
                          sh("${HTTPS_PROXY} git remote set-url origin https://${GIT_USERNAME}:${GIT_PASSWORD}@bitbucket.org/${BITBUCKET_SLUG}/${ROLE_NAME}.git")
                          sh("${HTTPS_PROXY} git push -u origin master ")
                          currentBuild.result = 'SUCCESS'
                       }


                    } // end dir
                }
            }
        } //end Commit changes to Bitbucket
    

        /*
           Stage calls another Jenkins pipeline/jobs
        */
       stage('Call build job'){
            when {
                expression {
                        currentBuild.result == 'SUCCESS' 
                }
            }
            // user input if we really want to trigger next job

          
            steps {
                    script {

                    build(job:"${BUILD_PIPELINE_JOB_NAME}",
                    parameters:
                        [string(name: 'cookbook_type', value: "app"), string(name: 'cookbook_name', value: "${ROLE_NAME}"),
                                  ])
                        // set build status
                        currentBuild.result = 'SUCCESS'
                }
            }
        }

    } // end stage
    

    /*
      Post actions
    */
    // cleanup WORKDIR and notify via email
    post {
        always {
            deleteDir()
        }
    
        success{
           send_email_notification(INFO_EMAIL,'Successful') 
        }

        unstable{
           send_email_notification(INFO_EMAIL,'Unstable')
        }

        failure {
           send_email_notification(INFO_EMAIL,'Failed')
        }
    }

} //end pipeline
